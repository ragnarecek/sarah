import Ember from 'ember';

export default Ember.Object.extend({
  type: '',
  subtype: '',
  subject_determiner: '',
  subject: 'I',
  object_determiner: '',
  object: null,
  target: 'Martin',
  predicate: '',
  time: 'present',
  location: 'here',
  formality: 'neutral'
});
