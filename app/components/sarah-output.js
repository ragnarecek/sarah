import Ember from 'ember';

export default Ember.Component.extend({
  SarahAI: Ember.inject.service('sarah-ai'),
  fullChat: [],
  init() {
    this._super.apply(this, arguments);
    this.set('fullChat', this.get('SarahAI').fullChat);
  },
  fullChatChanged: Ember.observer('fullChat.[]', function() {
    setTimeout(function(){
      Ember.$('.sarah-output-child').animate({scrollTop:Ember.$('.sarah-output-child')[0].scrollHeight}, 'fast');
    }, 100);
  }),
});
