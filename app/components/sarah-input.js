import Ember from 'ember';

export default Ember.Component.extend({
  SarahAI: Ember.inject.service('sarah-ai'),
  inputValue: '',
  actions: {
    inputSubmitted: function(input){
      this.get('SarahAI').processInput(input);
      this.set('inputValue', '');
    }
  }
});
