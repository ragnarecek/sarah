import Ember from 'ember';
import Thought from '../brain/thought';

export default Ember.Service.extend({

  context: 'Input Processor',
  log: Ember.inject.service('ai-logger'),
  memory: Ember.inject.service('ai-memory'),
  sarah_ai: Ember.inject.service('sarah-ai'),

  getStringToArray: function (string) {
    return string.trim().split(' ');
  },

  getArrayToPartsOfSpeech: function (array_of_strings) {
    var that = this;
    return Ember.RSVP.Promise.all(
      array_of_strings.map(function (value) {
        var searchable_word = value.toLowerCase(); // searchable word only

        return that.get('memory').findNodes(['Word'],{word:searchable_word}).then(function(nodes) {
          if (nodes) {
            return nodes.get('lastObject');
          } else {
            return null;
          }
        });
      })
    );
  },

  getAWordType: function(word) {
    var
      context = this.get('context') + '| getAWordType',
      labels = word.get('labels'),
      types = ['Verb', 'Adverb', 'Noun', 'Interjection', 'Adjective', 'Pronoun', 'Preposition', 'Conjunction', 'Determiner'];

    if(labels && labels.length) {
      for (let i = 0; i < labels.length; i++) {
        if (types.indexOf(labels[i]) !== -1) {
          return labels[i];
        }
      }
    }
    this.get('log').sarahErrors(context, 'No word type was found.');
    return null;
  },

  getAThought: function(parts_of_speech_array) {
    var
      context = this.get('context') + '| getAnalysedPartsOfSpeech',
      that = this,
      thought = new Thought();

    return new Ember.RSVP.Promise(function(resolve) {

      for(var i = 0; i < parts_of_speech_array.length; i++) {

        var value = parts_of_speech_array[i];

        if (!value) {
          continue;
        }

        switch (that.getAWordType(value)){

          case 'Interjection':
            if(value.get('labels').indexOf('Greeting') !== -1) {
                if (thought.type !== '') {
                  that.get('log').sarahThinks(context, 'I think I found another sentence on position ' + i +
                    ' with a word \'' + value.get('word') + '\'.');
                  that.get('sarah_ai').processSentence(parts_of_speech_array.slice(i));
                  break;
                }
                thought.type = 'exclamation';
                thought.subtype = 'greeting';
                thought.formality = value.get('properties').formality;
                that.get('log').sarahThinks(context, 'I think I received a greeting.');
                break;
            }
            break;

          case 'Adverb':

            if(value.get('labels').indexOf('Interrogative') !== -1) {
                if (thought.type !== '') {
                  that.get('log').sarahThinks(context, 'I think I found another sentence on position ' + i +
                    ' with a word \'' + value.get('properties').word + '\'.');
                  that.get('sarah_ai').processSentence(parts_of_speech_array.slice(i));
                  break;
                }
                thought.type = 'question';
                thought.subtype = value.get('properties').subject;
                that.get('log').sarahThinks(context, 'I think I received a question.');
            }
            break;

          case 'Verb':

            var be_forms = ['be', 'am', 'are', 'is', 'was', 'were', 'been'];
            if (be_forms.indexOf(value.get('properties').word) !== -1) {
              thought.predicate = 'be';
              thought.time = value.get('properties').time;
            } else {
              thought.predicate =  value.get('properties').word;
            }
            break;

          case 'Noun':

            thought.subject = value.get('properties').word;
            break;

          case 'Pronoun':

            if(value.get('labels').indexOf('Interrogative') !== -1) {
              if (thought.type !== '') {
                that.get('log').sarahThinks(context, 'I think I found another sentence on position ' + i +
                  ' with a word \'' + value.get('properties').word + '\'.');
                that.get('sarah_ai').processSentence(parts_of_speech_array.slice(i));
                break;
              }
              // word what
              thought.type = 'question';
              thought.subtype = (value.get('properties').subject) ? value.get('properties').subject : '';
            } else {
              switch(value.get('properties').word) {
                case 'you':
                  thought.subject = 'I';
                  break;
                case 'he':
                case 'she':
                case 'it':
                  thought.subject = value.get('properties').word;
                  break;
                default:
                  that.get('log').sarahErrors(context, 'I found unknown Pronoun.');
                  break;
              }
              break;
            }
            break;

          case 'Determiner':

            if(thought.subject === 'I' || thought.subject === '') {
              thought.subject_determiner = value.get('properties').definiteness;
            } else {
              thought.object_determiner = value.get('properties').definiteness;
            }
            break;

          default:
            that.get('log').sarahErrors(context, 'I received unknown word type.');
            break;
        }
      }
      resolve (thought);
    });
  }

});
