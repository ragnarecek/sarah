import Ember from 'ember';
//import Thought from '../brain/thought';

export default Ember.Service.extend({

  context: 'Sarah\'s AI',
  log: Ember.inject.service('ai-logger'),
  input_processor: Ember.inject.service('input-processor'),
  thought_processor: Ember.inject.service('thought-processor'),
  output_processor: Ember.inject.service('output-processor'),
  voice: Ember.inject.service('voice'),
  fullInput: [],
  fullChat: [],
  synchronisation_queue: new Ember.RSVP.Promise(function(resolve) {
    resolve(true);
  }),
  synchronisation_queue_counter: 0,

  init() {
    // this has to be called bacause of Chrome for some reason
    this.get('voice').init();
  },

  processInput: function (rawInputString) {
    var context = this.get('context') + '| processInput';
    var that = this;

    this.get('log').sarahThinks(context, 'I received raw input String: \'' + rawInputString + '\'.');
    this.get("fullChat").pushObject(rawInputString);

    // converting input string to an array of separate words
    var input_split = this.get('input_processor').getStringToArray(rawInputString);

    this.get('log').sarahThinks(context, 'I am processing ' + input_split.length + ' word(s).' );

    // converts array of string of words into objects of parts of speech (nouns, interjections etc.)
    this.get('input_processor').getArrayToPartsOfSpeech(input_split).then(function(parts_of_speech_array){

      // processes a sentence using synchronisation queue, creates cause and action to create and push output
      that.processSentence(parts_of_speech_array);

    });
  },

  processSentence: function (parts_of_speech_array) {
    var context = this.get('context') + '| processSentence';
    var that = this;
    return this.set('synchronisation_queue', that.get('synchronisation_queue').then(function () {
      that.increaseSynchronisationQueueCounter();
      that.get('log').sarahThinks(context, 'I am processing sentence number: ' + that.get('synchronisation_queue_counter') + '.');

      return that.get('input_processor').getAThought(parts_of_speech_array).then(function(thought){

        that.get('log').sarahThinks(context, 'I am processing a thought: \'' + thought.type + '\'.');
        that.get('log').sarahRemarks(context, thought);

        return that.get('thought_processor').getProcessedAction(thought);

      }).then(function(action){

        that.get('log').sarahThinks(context, 'I decided that my action is: \'' + action.type + '\'.');
        return that.get('output_processor').getActionSentence(action);

      }).then(function(sentence){

        that.get("fullChat").pushObject(sentence);

        that.get('voice').speak(sentence);

      });
    }));
  },

  increaseSynchronisationQueueCounter: function () {
    this.set('synchronisation_queue_counter', this.get('synchronisation_queue_counter') +1);
  },

});
