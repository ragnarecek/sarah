import Ember from 'ember';

export default Ember.Service.extend({

  context: 'AI Memory',
  database: Ember.inject.service('store'),
  log: Ember.inject.service('ai-logger'),

  joinJSON: function (obj1, obj2) {
    var finalobj = {};
    for(var _obj1 in obj1) {
      finalobj[_obj1] = obj1[_obj1];
    }
    for(var _obj2 in obj2) {
      finalobj[_obj2] = obj2[_obj2];
    }
    return finalobj;
  },

  findNodes: function (labels, properties, relationship) {
    var
      context = this.get('context') + '| findNodes',
      that = this;

    var parameters = (labels && labels.length > 0) ? { label: labels } : {};
    parameters = (properties) ? this.joinJSON(parameters, properties) : parameters;
    parameters = (relationship) ? this.joinJSON(parameters, { relationship: relationship }) : parameters;

    return this.get('database').query('node', parameters).then(
      function (nodes) {
        return nodes;
      },
      function (error) {
        if(""+(error) !== "TypeError: Cannot read property 'constructor' of null") { // means that no record was found
          that.get('log').sarahErrors(context, 'My memory encountered error: ' + error);
        }
        return null;
      }
    );
  }



});
