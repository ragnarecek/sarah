import Ember from 'ember';

export default Ember.Service.extend({

  context: 'Voice',
  log: Ember.inject.service('ai-logger'),
  voice: null,

  init() {
    var context = this.get('context') + '| init';
    this._super();
    // uses HTML5 text to speech standard
    if (typeof SpeechSynthesisUtterance !== 'undefined' && typeof speechSynthesis !== 'undefined') {
      var msg = new SpeechSynthesisUtterance();
      speechSynthesis.getVoices();
      this.set('voice', msg);
      this.get('log').sarahRemarks(context, 'I initialized voice.');
    } else {
      this.get('log').sarahWarns(context, 'Voice is not supported.');
    }
  },

  speak: function (sentence) {
    var context = this.get('context') + '| speak';
    if (!sentence) {
      this.get('log').sarahErrors(context, 'There was an attempt to speak while providing an empty sentence.');
      return;
    }
    // uses HTML5 text to speech standard
    if (typeof speechSynthesis !== 'undefined') {
      this.get('voice').voice = speechSynthesis.getVoices().filter(function(voice) { return voice.name === 'Fiona'; })[0];
      this.get('voice').rate = 0.8;
      this.get('voice').text = sentence;
      this.get('log').sarahRemarks(context, 'I am speaking using my voice.');
      speechSynthesis.speak(this.get('voice'));
    } else {
      this.get('log').sarahWarns(context, 'Voice is not supported, Sarah cannot speak.');
    }
  }

});
