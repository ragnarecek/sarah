import Ember from 'ember';
import Thought from '../brain/thought';

export default Ember.Service.extend({

  context: 'Thought Processor',
  log: Ember.inject.service('ai-logger'),
  memory: Ember.inject.service('ai-memory'),

  getProcessedAction: function (thought) {
    //var context = this.get('context') + '| getProcessedReaction';
    var that = this;
    return new Ember.RSVP.Promise(function(resolve) {
      switch (thought.type) {
        case 'exclamation':
          switch (thought.subtype) {
            case 'greeting':
              resolve(that.getGreetingAction(thought));
              break;
          }
          break;
        case 'question':
          resolve(that.getQuestionAction(thought));
          break;
        default:
          resolve(that.getUnknownThought());
      }
    });
  },

  getGreetingAction: function (thought) {
    var action = new Thought();
    action.type = 'exclamation';
    action.subtype = 'greeting';
    action.subject = 'I';
    action.target = 'Martin';
    action.location = 'there';
    action.formality = thought.formality;
    return action;
  },

  getQuestionAction: function (thought) {
    var action = new Thought();
    var that =  this;

    // find specification
    if(thought.subtype === 'object' && thought.subject_determiner === 'definitive') {
      return this.get('memory').findNodes(['Word','Noun'],{word:thought.subject},'is_specified_by').then(function (nodes) {
        return that[nodes.get('lastObject').get('properties').name](thought);
      });
    }

    action.type = 'declaration';
    action.subtype = 'rejection';
    action.subject = 'I';
    action.object = thought;
    action.object.type = 'definition';
    action.target = 'Martin';
    action.predicate = 'know';
    action.formality = thought.formality;

    return action;
  },

  date: function (thought) {
    var action = new Thought();
    action.type = 'declaration';
    action.subtype = 'object';
    action.subject_determiner = "definitive";
    action.subject = 'time';
    action.predicate = 'be';
    action.time = 'present';
    action.formality = thought.formality;

    if(thought.subject === 'time') {
      action.object = new Date().toLocaleTimeString();
    } else {
      action.object = new Date().toLocaleString();
    }

    return action;
  },

  getUnknownThought: function () {
    var action = new Thought();
    var object = new Thought();

    object.type = 'definition';
    object.subject = 'I';
    object.subtype = 'manner';
    object.predicate = 'react';
    object.time = 'infinitive';

    action.type = 'declaration';
    action.subject = 'I';
    action.subtype = 'rejection';
    action.predicate = 'know';
    action.object = object;
    action.target = 'Martin';

    return action;
  }

});
