import Ember from 'ember';

export default Ember.Service.extend({
  sarahThinks: function (context, thought) {
    Ember.Logger.info(context, ':', thought);
  },

  sarahRemarks: function (context, remark) {
    Ember.Logger.debug(context, ':', remark);
  },

  sarahWarns: function (context, warning) {
    Ember.Logger.warn(context, ':', warning);
  },

  sarahErrors: function (context, error) {
    Ember.Logger.error(context, ':', error);
  }
});
