import Ember from 'ember';
//import Thought from '../brain/thought';

export default Ember.Service.extend({

  context: 'Output Processor',
  log: Ember.inject.service('ai-logger'),
  memory: Ember.inject.service('ai-memory'),
  default_error: 'I am confused',

  getActionSentence: function (action) {
    var that = this;
    return new Ember.RSVP.Promise(function(resolve) {
      switch (action.type) {
        case 'exclamation':
          switch (action.subtype) {
            case 'greeting':
              that.getGreetingSentence(action).then(function (greeting) {
                resolve(greeting);
              });
              break;
          }
          break;
        case 'declaration':
          switch (action.subtype) {
            case 'rejection':
              resolve(that.getRejectionSentence(action));
              break;
            case 'object':
              that.getSpecificationSentence(action).then(function (sentence) {
                resolve(sentence);
              });
              break;
          }
          break;
      }
    });
  },

  getWordWithProbability: function (word, probability) {
    if(Math.random() < probability) {
      return (word.charAt(0) === ',') ? word :' ' + word;
    } else {
      return '';
    }
  },

  getCapitalizedFirstLetter: function (string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  },

  getGreetingSentence: function (action) {
    var that = this;
    var sentence = '';

    return new Ember.RSVP.Promise(function(resolve) {
      if(Math.random() < 0.3) {
        // Good morning :)
        var hours = new Date().getHours();
        var greeting = 'Good ';
        if (hours < 6) {
          greeting += (Math.random() < 0.5) ? 'early ' : '';
          greeting += 'morning';
        } else if (hours < 12) {
          greeting += 'morning';
        } else if (hours < 17) {
          greeting += 'afternoon';
        }  else if (hours < 21) {
          greeting += 'evening';
        } else if (hours < 24) {
          greeting += (Math.random() < 0.5) ? 'dlate ' : '';
          greeting += 'evening';
        }
        resolve(greeting);

      } else {
        var response_greetings = [];
        return that.get('memory').findNodes(['Word','Greeting']).then(function (records) {
          if (records.content.length === 0) {
            sentence = 'Hi';
            return;
          }
          records.forEach(function(value) {
            if(value.get('properties').formality === action.formality) {
              response_greetings.push(value.get('properties').word);
            }
          });


          resolve(that.getCapitalizedFirstLetter(response_greetings[Math.floor(Math.random()*response_greetings.length)]));

        });
      }
    }).then(function(greeting) {
      sentence += greeting;
      sentence += (action.location) ? that.getWordWithProbability(action.location, 0.1) : '';
      sentence += (action.target) ? that.getWordWithProbability(', ' + action.target, 0.8) : '';
      if (Math.random() < 0.1) {
        sentence += '!';
      } else {
        sentence += '.';
      }
      return sentence;
    });
  },

  getRejectionSentence: function (action) {
    var sentence = '';
    if (action.predicate !== '') {
      sentence += action.subject;
      sentence += ' don\'t ' + action.predicate;

      if (action.object instanceof Ember.Object) {
        sentence += ', ' + this.getThoughtObjectToSentence(action.object);
      }

      sentence += (action.object) ? this.getWordWithProbability(', ' + action.target, 0.1) : '';
      sentence += '.';
    } else {
      sentence = 'No.';
    }
    return sentence;
  },

  getReplaceSubjectWithPronoun(subject) {
    var sentence = '',
        that = this;
    return new Ember.RSVP.Promise(function(resolve) {
      return that.get('memory').findNodes(['Word', 'Noun'], {word: subject}).then(function (nodes) {
        var sex = nodes.get('lastObject').get('properties').sex;
        if (sex) {
          switch (sex) {
            case "male":
              sentence = "He";
              break;
            case "female":
              sentence = "She";
              break;
            default:
              sentence = "It";
              break;
          }
        } else {
          sentence = "It";
        }
        resolve(sentence);
        //return sentence;
      }, function () {
        sentence = "It";
        //resolve(sentence);
        return sentence;
      });
    });
  },

  getSpecificationSentence: function (action) {
    var sentence = '',
      that = this;
    sentence += " " + that.getVerbForm(action.predicate, 'present', 3, 'singular');
    sentence += " " + action.object + ".";
    return new Ember.RSVP.Promise(function(resolve) {
      if (action.subject_determiner === "definitive") {
        if (that.getWordWithProbability('word', 0.8)) {
          resolve(that.getReplaceSubjectWithPronoun(action.subject).then(function (result) {
            sentence = result + sentence;
            return sentence;
          }));
        } else {
          sentence = 'The ' + action.subject + sentence;
          resolve(sentence);
        }
      }
    });
  },

  getThoughtObjectToSentence: function (object) {
    var sentence = '';

    switch (object.type) {
      case 'definition':
        var verb_person = (object.subject === 'I') ? 1 : 3;
        switch (object.subtype) {
          case 'manner':
            if (object.time === 'present') {
              sentence = 'how ' + object.subject + ' ' + this.getVerbForm(object.predicate, 'present', verb_person, 'singular');
            } else { // infinitive
              sentence = 'how to ' + object.predicate;
            }
            break;
          case 'location':
            sentence = 'where ' + object.subject + ' ' + this.getVerbForm(object.predicate, 'present', verb_person, 'singular');
            break;
          case 'reason':
            sentence = 'why ' + object.subject +  ' ' + this.getVerbForm(object.predicate, 'present', verb_person, 'singular');
            break;
          case 'time':
            sentence = 'when ' + object.subject +  ' ' + this.getVerbForm(object.predicate, 'present', verb_person, 'singular');
            break;
          case 'object':
            sentence = 'what ' + object.subject +  ' ' + this.getVerbForm(object.predicate, 'present', verb_person, 'singular');
            break;
          case 'person':
            sentence = 'who ' + object.subject +  ' ' + this.getVerbForm(object.predicate, 'present', verb_person, 'singular');
            break;
          default:
            sentence = this.default_error;
        }
        break;
      default:
        sentence = this.default_error;
    }

    return sentence;
  },

  // 'be', 'present', 3, 'singular' => 'is'
  getVerbForm: function (verb, time, person, singular_plural) {
    var form = verb;
    if (verb === 'be') {
      switch (time) {
        case 'present':
          if (singular_plural === 'singular') {
            switch (person) {
              case 1:
                form = 'am';
                break;
              case 3:
                form = 'is';
                break;
              default:
                form = 'are';
            }
          } else {
            form = 'are';
          }
          break;
        case 'present continuous':
          break;
        case 'present perfect':
          break;
        case 'past':
          break;
        case 'future':
          break;
      }
    } else {
      switch (time) {
        case 'present':
          if (singular_plural === 'singular' && person === 3) {
              form = verb + 's';
          }
          break;
        case 'present continuous':
          break;
        case 'present perfect':
          break;
        case 'past':
          break;
        case 'future':
          break;
      }
    }
    return form;
  }

});
