import Ember from 'ember';

export default Ember.Route.extend({

  model: function () {
    return this.store.createRecord('word');
  },

  setupController: function (controller, model) {
    this._super(controller, model);

    controller.set('title', 'Create a new word');
    controller.set('buttonLabel', 'Create');
  },

  renderTemplate() {
    this.render('words/form');
  },

  actions: {

    saveWord(newWord) {
      newWord.save().then(() => this.transitionTo('words'));
    },

    willTransition() {
      let model = this.controller.get('model');

      if (model.get('isNew')) {
        model.destroyRecord();
      }
    }
  }
});
