import Ember from 'ember';

export default Ember.Route.extend({

  model(params) {
    return this.store.findRecord('word', params.word_id);
  },

  setupController(controller, model) {
    this._super(controller, model);

    controller.set('title', 'Edit word');
    controller.set('buttonLabel', 'Save changes');
  },

  renderTemplate() {
    this.render('words/form');
  },

  actions: {

    saveWord(newWord) {
      newWord.save().then(() => this.transitionTo('words'));
    },

    willTransition(transition) {
      let model = this.controller.get('model');

      if (model.get('hasDirtyAttributes')) {
        let confirmation = confirm("Your changes haven't saved yet. Would you like to leave this form?");

        if (confirmation) {
          model.rollbackAttributes();
        } else {
          transition.abort();
        }
      }
    }
  }
});
