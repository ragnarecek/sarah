import Ember from 'ember';
//import FirebaseAdapter from 'emberfire/adapters/firebase';

const { inject } = Ember;

/*export default FirebaseAdapter.extend({
  firebase: inject.service(),
});*/

export default DS.JSONAPIAdapter.extend({
  namespace: 'api/v1',
  host: 'https://sarah-ai.herokuapp.com',
  pathForType: function(type) {
    return Ember.String.singularize(type);
  }
});
